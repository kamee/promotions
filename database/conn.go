package database

import (
    "os"
    "fmt"
    "log"
    "context"
    "sync"
    "github.com/jackc/pgx/v5/pgxpool"
)

type Repository struct {
    Pool *pgxpool.Pool
}

var (
    pgInstance *Repository
    pgOnce     sync.Once
)

func dsn() string {
    username := os.Getenv("DB_USERNAME")
    password := os.Getenv("DB_PASSWORD")
    hostname := os.Getenv("DB_HOST")
    db_name  := os.Getenv("DB_NAME")

    return fmt.Sprintf("postgres://%s:%s@%s:5432/%s", username, password, hostname, db_name)
}

func Connection(ctx context.Context) (*Repository, error) {
    pgOnce.Do(func() {
	db, err := pgxpool.New(ctx, dsn())
	if err != nil {
	    log.Fatal(err)
	}

	pgInstance = &Repository{db}
    })

    return pgInstance, nil
}
