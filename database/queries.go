package database

import (
    "fmt"
    "log"
    "context"
    "github.com/jackc/pgx/v5"
)

func (db *Repository) CreateTablePromotions(ctx context.Context, tx pgx.Tx) error {
     // TODO: fix data type price should be float64
    _query := fmt.Sprintf(`create table if not exists promotions(
	    id SERIAL PRIMARY KEY,
	    promotion_id text,
            price text,
            expiration_date text
    );`)

    if _, err := tx.Exec(ctx, _query); err != nil {
	log.Println(err)
	return err
    }

    return nil
}

func (db *Repository) CreateIndex(ctx context.Context, tx pgx.Tx) error {
    _query := fmt.Sprintf(`create index on promotions(promotion_id);`)
    if _, err := tx.Exec(ctx, _query); err != nil {
        return err
    }

    return nil
}

func (db *Repository) InsertPromotions(ctx context.Context, tx pgx.Tx, id, price, expiration_date string) error {
    _query := fmt.Sprintf(`
        insert into promotions(promotion_id, price, expiration_date) values ($1, $2, $3);`)

    if _, err := tx.Exec(ctx, _query, id, price, expiration_date); err != nil {
        return err
    }

    return nil
}

func (db *Repository) FetchById(ctx context.Context, tx pgx.Tx, id int) (promotion_id, price, expiration_date string, err error){
    _query := fmt.Sprintf(`
        select promotion_id, price, expiration_date from promotions where id = $1;`)

    if err = tx.QueryRow(ctx, _query, id).Scan(&promotion_id, &price, &expiration_date); err != nil {
	log.Println(err)
        return
    }

    return 
}

func (db *Repository) FetchByPromotionId(ctx context.Context, tx pgx.Tx, id string) (promotion_id, price, expiration_date string, err error){
    _query := fmt.Sprintf(`
        select promotion_id, price, expiration_date from promotions where promotion_id = $1;`)

    if err = tx.QueryRow(ctx, _query, id).Scan(&promotion_id, &price, &expiration_date); err != nil {
	log.Println(err)
        return
    }

    return 
}
