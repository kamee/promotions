package main

import (
    "log"
    "net/http"
    "bufio"
    "context"
    "strconv"
    "strings"
    "sync"
    "encoding/json"
    "gitlab.com/kamee/verve/database"

    "github.com/gorilla/mux"
    "github.com/jackc/pgx/v5"
)

var db *database.Repository

type Record struct {
    ID string `json:"id"`
    Price string `json:"price"`
    Date string `json:"expiration_date"`
}

func main(){
    ctx := context.Background()

    var err error
    db, err = database.Connection(ctx)
    if err != nil {
        log.Println(err)
    }

    beginTx(ctx, db, func(tx pgx.Tx) error {
        err := db.CreateTablePromotions(ctx, tx)
        if err != nil {
	    log.Println(err)
        }

        return err
    })

    beginTx(ctx, db, func(tx pgx.Tx) error {
        err := db.CreateIndex(ctx, tx)
        if err != nil {
	     log.Println(err)
        }

        return err
    })

    routes := mux.NewRouter()
    port := ":8080"

    routes.HandleFunc("/getcsv", csv_handler)
    routes.HandleFunc("/promotions/{id}", promotions_handler)
    routes.HandleFunc("/promotion/{id}", id_handler)
    log.Fatal(http.ListenAndServe(port, routes))
}

func writeondb(ctx context.Context, data []string){ 
     beginTx(ctx, db, func(tx pgx.Tx) (err error) {
      err = db.InsertPromotions(ctx, tx, data[0], data[1], data[2])
      if err != nil {
           log.Println(err)
       }

       return nil
       })
}

func csv_handler(w http.ResponseWriter, r *http.Request){
    type processed struct {
	ids  []string
    }

    numWorkers := 10
    reader := func(ctx context.Context, rowsBatch *[]string) <-chan []string {
        out := make(chan []string)
	batchSize := 1000

        scanner := bufio.NewScanner(r.Body)
        
	go func() {
            defer close(out)

            for {
                scanned := scanner.Scan()

                select {
                    case <-ctx.Done():
                         return
                    default:
                         row := scanner.Text()
                         if len(*rowsBatch) == batchSize || !scanned {
                            out <- *rowsBatch
                            *rowsBatch = []string{}
                        }
                            *rowsBatch = append(*rowsBatch, row)
                    }

                  if !scanned {
                        return
                  }
               }
                }()

                return out
        }

	worker := func(ctx context.Context, rowBatch <-chan []string) <-chan processed {
            out := make(chan processed)
	    //data := make(chan []string)

            go func() {
                defer close(out)

                p := processed{}
                for rowBatch := range rowBatch {
                    for _, row := range rowBatch {
	                res1 := strings.Split(row, ",")
			// go func() {
                        //   data <- res1
			// }()


	                // msg := <-data
	                // go writeondb(ctx, msg)
                        writeondb(ctx, res1)

                    }
                }
                
		out <- p
            }()

	    return out
        }

	combiner := func(ctx context.Context, inputs ...<-chan processed) <-chan processed {
            out := make(chan processed)

            var wg sync.WaitGroup
            multiplexer := func(p <-chan processed) {
            defer wg.Done()

                for in := range p {
                    select {
                    case <-ctx.Done():
                    case out <- in:
                    }
                }
            }

            wg.Add(len(inputs))
            for _, in := range inputs {
                go multiplexer(in)
            }

            go func() {
                wg.Wait()
                close(out)
            }()

            return out
        }

        ctx := r.Context()
	rowsBatch := []string{}

        rowsCh := reader(ctx, &rowsBatch)

	workersCh := make([]<-chan processed, numWorkers)
        for i := 0; i < numWorkers; i++ {
            workersCh[i] = worker(ctx, rowsCh)
        }

	var IdCount map[string]interface{}
	for processed := range combiner(ctx, workersCh...) {
            for _, id := range processed.ids {
                IdCount[id] = true
            }
        }
}

func promotions_handler(w http.ResponseWriter, r *http.Request){
    vars := mux.Vars(r)
    _id, ok := vars["id"]
    if !ok {
        http.Error(w, "Bad Request", http.StatusBadRequest)
	return
    }

    id, err := strconv.Atoi(_id)
    if err != nil {
	log.Println(err)
    }

    var pr_id, price, expiration_date string
    err = beginTx(r.Context(), db, func(tx pgx.Tx) (err error) {
	pr_id, price, expiration_date, err = db.FetchById(r.Context(), tx, id)
	if err != nil {
	    if err == pgx.ErrNoRows {
	        http.Error(w, "Not Found", http.StatusNotFound)
		return
	    }

	    http.Error(w, "Bad Request", http.StatusBadRequest)
	    return
        }

	return
    })

    if err == nil {
        data := &Record{
            ID: pr_id,
	    Price: price,
	    Date: expiration_date,
        }

        w.Header().Set("Content-Type", "application/json")
        json.NewEncoder(w).Encode(data)
    }
}

func id_handler(w http.ResponseWriter, r *http.Request){
    vars := mux.Vars(r)
    id, ok := vars["id"]
    if !ok {
        http.Error(w, "Bad Request", http.StatusBadRequest)
	return
    }

    var pr_id, price, expiration_date string
    err := beginTx(r.Context(), db, func(tx pgx.Tx) (err error) {
	pr_id, price, expiration_date, err = db.FetchByPromotionId(r.Context(), tx, id)
        if err != nil {
	    if err == pgx.ErrNoRows {
		http.Error(w, "Not Found", http.StatusNotFound)
		return
	    }

	    http.Error(w, "Bad Request", http.StatusBadRequest)
	    return
        }



	return nil
    })
    
    if err == nil {
        data := &Record{
            ID: pr_id,
	    Price: price,
	    Date: expiration_date,
        }

        w.Header().Set("Content-Type", "application/json")
        w.WriteHeader(http.StatusOK)
        json.NewEncoder(w).Encode(data)
    }
}

func beginTx(ctx context.Context, db *database.Repository, fn func(tx pgx.Tx) error) error {
    tx, err := db.Pool.Begin(ctx)
    if err != nil {
        return err
    }

    if err = fn(tx); err != nil {
        return err
    }

    return tx.Commit(ctx)
}
