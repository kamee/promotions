# promotions

### Dependencies

Go 1.20

PostrgeSQL

github.com/jackc/pgx/v4

go get github.com/gorilla/mux

### Usage

1. export DB_USERNAME, DB_PASSWORD, DB_HOST, DB_NAME

2. go run main.go

### Endpoints

/getcsv -> for sending csv file

/promotions/{id} -> uses row N for searching

/promotion/{id} -> uses promotion id for searching


***

__tested on arch linux__
